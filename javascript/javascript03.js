//Funcion datos
function datos(parametro) {

    if (parametro % 2 === 0) {
        console.log("El numero " + parametro + " es par");
    } else {
        console.log("El numero " + parametro + " es inpar");
    }
    if (parametro % 3 === 0) {
        console.log("El numero " + parametro + " es divisible entre 3");
    } else {
        console.log("El numero " + parametro + " no es divisible entre 3");
    }
    if (parametro % 5 === 0) {
        console.log("El numero " + parametro + "  es divisible entre 5");

    } else {
        console.log("El numero " + parametro + " no es divisible entre 5");
    }
    if (parametro % 7 === 0) {
        console.log("El numero " + parametro + "es divisible entre 7");
    } else {
        console.log("El numero " + parametro + " no es divisible entre 7");
    }
}
datos(1240);

//Funcion suma valores
function sumaValores(arr) {
    var sumaTotal = 0;
    for (i = 0; i < arr.length; i++) {
        sumaTotal += arr[i];
    }
    console.log("La suma es " + sumaTotal);
}

sumaValores([1, 3, 9, 2, 3]);

//calcular numero primo

function primo(numero) {
    var primo=false;
    var numerosDivisibles= [];
    var y=0;
    for (i = numero - 1; i != 1; i--) {
        if (numero % i === 0 && i !==1) {
            
            numerosDivisibles[y]=i;
            y++;
        
            //console.log((numero ) + " no es un numero primo porque " + (numero) + " es un multiplo de " + i);
            primo=true; 
            //break;
        }   
    }
    if (primo == false) {
        console.log("El numero " + numero + " es primo");
    }else{
        console.log((numero ) + " no es un numero primo porque " + (numero) + " es un multiplo de " + numerosDivisibles.reverse());
    }
    

}
primo(9);
primo(5);
primo(35);
primo(255);

function capitaliza (palabra){
    for(var i=0;i<palabra.length;i++){
        primeraLetra = palabra.substr(0,1);
        resto = palabra.substr(1);
        primeraLetra= primeraLetra.toUpperCase();
        resto= resto.toLowerCase();
        
    }
    console.log(primeraLetra+resto);
}
capitaliza ("hola");